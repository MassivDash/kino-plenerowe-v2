import React from 'react';
import Excerpt from './Excerpt/Excerpt';
import Picture from './Picture/Picture';
import Title from './Title/Title';


const Post = () => (
  // We can pull the environment from `process.env.NODE_ENV`, which is set
  // to either 'development' | 'production' on both the server and in the browser


  <div>
    <Title />
    <Picture />
    <Excerpt />
  </div>
);


export default Post;
