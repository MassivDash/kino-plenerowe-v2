import React from 'react';
import { history } from 'kit/lib/routing';

const GoBack = () => (
  <div className="">
    <button onClick={history.goBack}>Back</button>
  </div>
)

export default GoBack;