import React from 'react';
import scss from './Footer.scss';

const Footer = (props) => (
<footer>
    <div className={scss.Logo}></div>
    <div className={scss.copyright}>Designed and maintained by <a href="https://spaceout.pl">spaceout.pl</a></div>

</footer>

);

export default Footer;
