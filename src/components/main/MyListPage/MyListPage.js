import React, { Component } from 'react';
import { connect } from 'react-redux';
import MyMovie from './mymovie/MyMovie';


// ----------------------

// @connect accepts a function that takes the full Redux state, and then
// returns the portion of state that our component cares about.  In this example,
// we're listening to `state.counter`, which we can show inside the component


@connect(state => ({ movielist: state.movielist }))
class MyListPage extends Component {

 
    

render(){
    
    const Data  = this.props.movielist.movielist;
    let MyMovies = <div>Dodaj filmy do swojej listy aby zobaczyc je tutaj</div>
    
    if ( Data.length > 1 ) {
        MyMovies =  <div>
            { Data.map((posts, i) => <div key={i} ><MyMovie
        id={posts}
        
      /></div>) } </div>
  
    }

    

    return (
        <div>
            Moja Lista
            {MyMovies}

        </div>

    )
}
}


export default MyListPage