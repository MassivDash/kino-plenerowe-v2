import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import PropTypes from 'prop-types';
import Overdrive from 'react-overdrive';
import GetMyMovie from './MyMoviesPage.gql';
import scss from './Mymovie.scss';
import Spinner from '../../ui/spinner/Spinner';

/**
 * This is the div that contains the Details about the movie (poster, title, description, etc)
 */

@graphql(GetMyMovie)

class MyMoviePage extends Component {
  state= {
    playing: false,
    loading: true,
   
  }

              static propTypes = {
        data: PropTypes.shape({
          id: PropTypes.string,
          title: PropTypes.string,
          content: PropTypes.string,
          tags: PropTypes.shape({
              edges: PropTypes.array[{
                  node: PropTypes.shape({
                      name:PropTypes.string 
                  })
              }]
              }),
          termNames : PropTypes.array,
          excerpt: PropTypes.string,
            featuredImage: PropTypes.shape({
              sourceUrl: PropTypes.string,
            
          }),
        }),
      }

      static defaultProps = {
        data: {
          id: '',
          title: 'la la',
          post: {
            excerpt: 'lal lalala',
            featuredImage: {
              sourceUrl: 'https://plenerowekino.pl/wp-content/uploads/2017/08/dziewczyna-warta-grzechu.jpg',
            },
          },
        },
      }

  render() {

    const { data } = this.props;

  

    /**
     * Configure the posterUrl
     * @type {string}
     */
    let posterUrl = '';
    if (data.movie && data.movie.featuredImage && data.movie.featuredImage.sourceUrl) {
      posterUrl = data.movie.featuredImage.sourceUrl;
    }

    let SingleMovie =  (
      <Overdrive id={this.props.id}>
          <Spinner />
          </Overdrive>
      );
        
if (!data.loading) { 
      
  SingleMovie = (
  
  <div className={scss.MyWrapper}>
    <div className={scss.MyMoviePoster}>
    <Overdrive id={this.props.id}>
    <img src={posterUrl} alt={'poster for ' + data.movie.title} />
    </Overdrive>
    </div>
    <div className={scss.descprtion}>
    <p>{data.movie.title}, {data.movie.tags.edges[0].node.name}, 
    {data.movie.tags.edges[1].node.name}, {data.movie.excerpt}   
    </p>

        </div>
  </div>
  
)
          
    ;
    }

 
    
  
  

  return ( 
    <div>
                      {SingleMovie}
                      
     </div>
          );
          
      
      
        }
    };
    

/**
 * Here we query for a single post (movie) by passing the ID as a variable.
 */



export default graphql(GetMyMovie, {
  options: (props) => { return { variables: { id: props.id }}}
})(MyMoviePage);