import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import Overdrive from 'react-overdrive';
import PropTypes from 'prop-types';
import Spinner from './../ui/spinner/Spinner';
import GoBack from '../ui/goback/GoBack';
import GetMoviesPage from './MoviesPage.gql';
import Poster from './poster/Poster';
import Description from './description/Description';
import scss from './MoviesPage.scss';
import Toogle from '../mainpage/movies/mylistbutton/MyListButton';
/**
 * This is the div that contains the Details about the movie (poster, title, description, etc)
 */

@graphql(GetMoviesPage)

class MoviesPage extends Component {
  state= {
    playing: false,
    loading: true,
   
  }

              static propTypes = {
        data: PropTypes.shape({
          id: PropTypes.string,
          title: PropTypes.string,
          content: PropTypes.string,
          tags: PropTypes.shape({
              edges: PropTypes.array[{
                  node: PropTypes.shape({
                      name:PropTypes.string 
                  })
              }]
              }),
          termNames : PropTypes.array,
          excerpt: PropTypes.string,
            featuredImage: PropTypes.shape({
              sourceUrl: PropTypes.string,
            
          }),
        }),
      }

      static defaultProps = {
        data: {
          id: '',
          title: 'la la',
          post: {
            excerpt: 'lal lalala',
            featuredImage: {
              sourceUrl: 'https://plenerowekino.pl/wp-content/uploads/2017/08/dziewczyna-warta-grzechu.jpg',
            },
          },
        },
      }

  render() {

    const { data } = this.props;

  

    /**
     * Configure the posterUrl
     * @type {string}
     */
    let posterUrl = 'http://placehold.it/341x512';
    if (data.movie && data.movie.featuredImage && data.movie.featuredImage.sourceUrl) {
      posterUrl = data.movie.featuredImage.sourceUrl;
    }

    let SingleMovie =  (
      <Overdrive id={this.props.id}>
          <Spinner />
          </Overdrive>
      );
        
if (!data.loading) { 
      
  SingleMovie = (
  
  <div className={scss.PageWrapper}>
    <div className={scss.section_col4}>
    <Overdrive id={this.props.id}>
    <Poster poster={posterUrl} alt={'poster for ' + data.movie.title} />
    </Overdrive>
    </div>
    <div className={scss.section_col8}>
    <Description 
    title={data.movie.title}
    date={data.movie.tags.edges[0].node.name}
    place={data.movie.tags.edges[1].node.name}
    description={data.movie.excerpt}   
    />
    Dodaj film do swojej listy<Toogle />
    <GoBack />
        </div>
  </div>
  
)
          
    ;
    }

 
    
  
  

  return ( 
    <div>
                      {SingleMovie}
                      
     </div>
          );
          
      
      
        }
    };
    

/**
 * Here we query for a single post (movie) by passing the ID as a variable.
 */



export default graphql(GetMoviesPage, {
  options: (props) => { return { variables: { id: props.id }}}
})(MoviesPage);