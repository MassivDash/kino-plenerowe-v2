import React from 'react';
import scss from './Poster.scss';

const Poster = ( props ) => (
      
      
        <div className={scss.Poster}>
          <img src={props.poster} alt={props.alt} />
        </div>
  
      
    
  );
  
  
  export default Poster;