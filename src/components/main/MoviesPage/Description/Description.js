import React from 'react';
import scss from './Description.scss';

const Description = ( props ) => (
      
      <div className={scss.Description_Wrapper}>
        <div className={scss.Title}>
          {props.title}
        </div>
        <div className={scss.DateAndPlace}>
          {props.date}, {props.place} 
        </div>

        <div className={scss.Description}>
        <div dangerouslySetInnerHTML={{ __html: props.description}} />
        </div>
            
  
      </div>
    
  
  );
  
  
  export default Description;