import React from 'react';
import scss from './MainPost.scss';

const MainPost = (props) => (

<article className="container">

<blockquote>
    <strong> {props.title}</strong><p> <em>{props.content}</em></p><b> <strong>{props.description}</strong>
</b></blockquote>
<b>{props.date} </b>

</article>    


);

export default MainPost;