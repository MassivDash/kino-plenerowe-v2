import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import GetPost from './HeroSection.gql';
import MainPost from './mainpost/MainPost';
import Auxy from '../../../../hoc/Auxy';
import YouTube from './youtube/YouTube';
import Button from '../../ui/button/Button';
import play from '../../../../assets/svg/play.svg';
import Spinner from '../../ui/spinner/Spinner';
import scss from './HeroSection.scss';

@graphql(GetPost)


class HeroSection extends Component {
    state= {
      playing: false,
      loading: true
    }

                static propTypes = {
          data: PropTypes.shape({
            id: PropTypes.string,
            title: PropTypes.string,
            content: PropTypes.string,
            tags: PropTypes.shape({
                edges: PropTypes.array[{
                    node: PropTypes.shape({
                        name:PropTypes.string 
                    })
                }]
                }),
            termNames : PropTypes.array,
            post: PropTypes.shape({
              excerpt: PropTypes.string,
              featuredImage: PropTypes.shape({
                sourceUrl: PropTypes.string,
              }),
            }),
          }),
        }
      
        static defaultProps = {
          data: {
            id: '1',
            title: 'la la',
            post: {
              excerpt: 'lal lalala',
              featuredImage: {
                sourceUrl: 'https://plenerowekino.pl/wp-content/uploads/2017/08/dziewczyna-warta-grzechu.jpg',
              },
            },
          },
        }
    
        VideoPlayHandler = () => {
            this.setState({playing: true})
          }
        
          VideoStopHandler = () => {
            this.setState({playing: false})
          }
  
render () {
    const { data } = this.props;
    const MyPicture = data.post && data.post.featuredImage.sourceUrl;
    const MyTitle = data.post && data.post.title;
    const MyDate = data.post && data.post.termNames[0];
    const MyConetent = data.post && data.post.content;
    const MyDescription = data.post && data.post.excerpt;
    const MyYoutube = data.post && data.post.tags.edges[0].node.name;

    const style = {
        backgroundImage: "url("+MyPicture+")"
        
      }

    const MyDesc = MyDescription && MyDescription.replace(/<(?:.|\n)*?>/gm, '');
    const MyCont = MyConetent && MyConetent.replace(/<(?:.|\n)*?>/gm, '');
    
    let Hero = (
    <div className={scss.section__hero_container_video}>
        <Spinner />
    </div>
    );
    
    


    if (!data.loading) {    
    Hero = (
        <div className={scss.section__hero_container} style={style}>
            

    <div className={scss.section_hero_col8}>
    <MainPost title={MyTitle} description={MyDesc} content={MyCont} date={MyDate} />
    <Button
        btnType="PlayMe"
        clicked={this.VideoPlayHandler}
        >
        <img className={scss.play} src={play} alt="kino plenerowe" /><p>
        Odtwórz Trailer</p></Button>
    </div>
    <div className={scss.section_hero_col4}>
            
             
    </div>
    </div>
    
    )
}
    ;

    if (this.state.playing) {
        Hero = (
        <div className={scss.section__hero_container_video}>
        <YouTube video={MyYoutube} autoplay="1" rel="0" modest="1" />
        <Button
        btnType="StopMe"
        clicked={this.VideoStopHandler}
        >
        Zamknij trailer</Button>
        <Button
        btnType="StopMe"
        clicked={this.VideoStopHandler}
        >
        Przejdz do filmu</Button>          
        </div>
        );
    }
    
       
    return ( 
        <Auxy>
                          {Hero}
         </Auxy>
        );
          
      
      
    }
};


export default HeroSection;  