import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import AliceCarousel from 'react-alice-carousel';
import Movie from './movie/Movie';
import GetCategoryPosts from './MoviesDynamic.gql';
import scss from './Movies.scss';



@graphql(GetCategoryPosts)
@connect(state => ({ currentIndex: state.currentIndex }))
class MoviesDynamic extends Component {

    state = {
        currentIndex: 0,
        items: [1,2,3,4,5]
      };
    
      componentWillReceiveProps(nextProps) {
        if(nextProps.currentIndex !== this.props.currentIndex) {
            this.setState({ currentIndex: this.props.currentIndex.currentIndex });
        }
      }

      shouldComponentUpdate(nextProps, nextState) {
        return nextProps.currentIndex ==this.props.currentIndex || nextProps.children !== this.props.children;
      }
      
componentWillMount(){
    console.log(this.props)
    this.setState({ currentIndex: this.props.currentIndex.currentIndex });
} 

static propTypes = {
    counter: PropTypes.shape({
        count: PropTypes.number.isRequired,
      }),
            
          categories: PropTypes.shape({
            edges: PropTypes.array[{
                node: PropTypes.shape({
                    id: PropTypes.string,
                    name: PropTypes.string,
                    posts: PropTypes.shape({
                        edges: PropTypes.array[{
                            node: PropTypes.shape({
                                id: PropTypes.string,
                                title: PropTypes.string,
                                excerpt: PropTypes.string,
                                tags: PropTypes.shape ({
                                    edges: PropTypes.array[{
                                        node: PropTypes.shape ({
                                            name: PropTypes.string
                                        })
                                        
                                    }]
                                }),
                                featuredImage: PropTypes.shape ({
                                   sourceUrl: PropTypes.string 
                                })
                            })
                        }]
                    })
                })
            }]
                
        })
    }
      
        static defaultProps =  {
            counter: {
                count: 0,
              },
            categories: {
              edges: {
                  node: {
                      id: "hello",
                      name: "Hello",
                      posts: {
                          edges: {
                              node: {
                                  id: "1y298",
                                  title: "main",
                                  excerpt: "babksa",
                                  tags: {
                                      edges: {
                                          node: {
                                            name: "Kino"
                                          }
                                                                                }
                                  },
                                  featuredImage: {
                                     sourceUrl: "skjhaksa" 
                                  }
                              }
                            }
                      }
                  }
                }
                  
          }
      };
        

      onSlideChangedRedux = (e) => {
        this.props.dispatch({
            type: 'SLIDER_ADJUST',
            payload: e.item }); }
             
        
     
render () {
    const { data } = this.props;
    console.log(this.state)
    

    
    
         
    const MyData = data.categories && data.categories.edges[0].node.posts.edges;
  
    let MyPosts = (<div className={scss.Loading}> Wczytuje dostępne filmy ... </div>)
    
    const responsive = {
        0: {
          items: 3
        },
        600: {
          items: 4
        },
        1024: {
          items: 6
        }
      };
       
    
if (!data.loading) {
    const { data } = this.props;
    const MyData = data.categories && data.categories.edges[0].node.posts.edges;
    
    const responsive = {
        0: {
          items: 3
        },
        600: {
          items: 4
        },
        1024: {
          items: 5
        }
      };
  
    MyPosts = 
        <AliceCarousel
        fadeOutAnimation={true}
        startIndex = {this.state.currentIndex}
        onSlideChanged={this.onSlideChangedRedux}
        mouseDragEnabled={true}
        playButtonEnabled={false}
        responsive={responsive}
        dotsDisabled={true}

        
        
      >

      { MyData.map((posts, i) => <div key={i} ><Movie
        myid={posts.node.id}
        key={posts.node.id} 
        title={posts.node.title} 
        image={posts.node.featuredImage.sourceUrl}
        date={posts.node.tags.edges[0].node.name}
        place={posts.node.tags.edges[1].node.name}
        description= {posts.node.excerpt}   
      /></div>) }
      </AliceCarousel>;
    
      

};


return (
<div className={scss.Section}>
            
        <div className={scss.Section_Title}>{this.props.name}</div> 
        <div className={scss.Movies}>
        
        {MyPosts}
        
         </div>
        
         
</div>
        );
          
      
      
    }
};

export default graphql(GetCategoryPosts, {
    options: (props) => { return { variables: { "name": props.name }}}
  })(MoviesDynamic);