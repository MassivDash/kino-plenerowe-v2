import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import scss from './MyListButton.scss';

@connect(state => ({ movielist: state.movielist }))
class ListToggle extends Component {
static propTypes = {
  id: PropTypes.string,
  movielist: PropTypes.array,
	  };


	state = {
	  toggled: false,
	}



	handleClickRed = () => {
	  if (this.state.toggled === true) {
		this.setState({ toggled: false }),
		this.props.dispatch({
			type: 'REMOVE_TOMOVIELIST',
			payload: this.props.id,
		  })
		
	  } else {
	    this.setState({ toggled: true }),
	    this.props.dispatch({
			  type: 'ADD_TOMOVIELIST',
			  payload: this.props.id,
			})
		  };
	  }
	  
	
	  handleClick = () => {
		if (this.state.toggled === true) {
		  this.setState({ toggled: false })
		  
		} else {
		  this.setState({ toggled: true })
		  			};
		}
		

		componentDidMount(){
			let myListCheck = this.props.movielist.movielist.includes(this.props.id); 
		
		if (myListCheck) {
			this.handleClick()
		}
		}
			
		shouldComponentUpdate(nextProps, nextState) {
			return nextState.toggled !==this.props.toggled || nextState.children !== this.state.children;
		  }   		

			
	
	render() {
		
		
		

	  return (
		  
  <div onClick={this.handleClickRed} data-toggled={this.state.toggled} className={scss.ListToggle}>
    <div>
      <i><img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDQyIDQyIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA0MiA0MjsiIHhtbDpzcGFjZT0icHJlc2VydmUiIHdpZHRoPSIyNHB4IiBoZWlnaHQ9IjI0cHgiPgo8cG9seWdvbiBwb2ludHM9IjQyLDIwIDIyLDIwIDIyLDAgMjAsMCAyMCwyMCAwLDIwIDAsMjIgMjAsMjIgMjAsNDIgMjIsNDIgMjIsMjIgNDIsMjIgIiBmaWxsPSIjYmFiYWJhIi8+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=" alt="kino plenerowe warszawa" /></i>
      <i><img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeD0iMHB4IiB5PSIwcHgiIHZpZXdCb3g9IjAgMCA1MTEuOTk5IDUxMS45OTkiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMS45OTkgNTExLjk5OTsiIHhtbDpzcGFjZT0icHJlc2VydmUiIHdpZHRoPSIyNHB4IiBoZWlnaHQ9IjI0cHgiPgo8Zz4KCTxnPgoJCTxwYXRoIGQ9Ik01MDYuMjMxLDc1LjUwOGMtNy42ODktNy42OS0yMC4xNTgtNy42OS0yNy44NDksMGwtMzE5LjIxLDMxOS4yMTFMMzMuNjE3LDI2OS4xNjNjLTcuNjg5LTcuNjkxLTIwLjE1OC03LjY5MS0yNy44NDksMCAgICBjLTcuNjksNy42OS03LjY5LDIwLjE1OCwwLDI3Ljg0OWwxMzkuNDgxLDEzOS40ODFjNy42ODcsNy42ODcsMjAuMTYsNy42ODksMjcuODQ5LDBsMzMzLjEzMy0zMzMuMTM2ICAgIEM1MTMuOTIxLDk1LjY2Niw1MTMuOTIxLDgzLjE5OCw1MDYuMjMxLDc1LjUwOHoiIGZpbGw9IiNGRkZGRkYiLz4KCTwvZz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8L3N2Zz4K" alt="kino plenerowe warszawa" /></i>
    </div>
  </div>
	  );
	}
}

export default ListToggle;
