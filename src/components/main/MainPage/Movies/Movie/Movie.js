import React from 'react';
import Overdrive from 'react-overdrive'
import { Link }  from 'react-router-dom';
import ListToggle from '.././mylistbutton/MyListButton';
import scss from './Movie.scss';

const Movie = (props) => (
   
   <Overdrive id={props.myid} > 
     <div className={scss.movie_container} style= {{
        backgroundImage: "url("+props.image+")"
        
      }}>
        
     
        <div className={scss.overlay}>
        <Link 
      to={'/movies/' + props.myid} 
      alt={props.title + ' ' + props.date + ' ' + props.place} 
      className={scss.link}>    
        <div className={scss.title}>{props.title}</div>
        <div className={scss.date}>{props.date}</div>
        <div className={scss.description} dangerouslySetInnerHTML={{__html: props.description}} />
        <div className={scss.place}>{props.place}</div>
        </Link>
        <ListToggle id={props.myid}/>
        </div>
        
    </div>
   </Overdrive>

);



export default Movie
