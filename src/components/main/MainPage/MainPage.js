import React from 'react';
import HeroSection from './herosection/HeroSection';
import MoviesDynamic from './movies/MoviesDynamic';
import Auxy from '../../..//hoc/Auxy';

const MainPage = () => (
<Auxy>
<HeroSection />
<MoviesDynamic name="Archiwum" />
<MoviesDynamic name="KinoPlenerowe" />
</Auxy>
);

export default MainPage
