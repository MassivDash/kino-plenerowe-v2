import React from 'react';
import { NavLink, Link } from 'react-router-dom';
import AUXY from '../../../hoc/Auxy';
import scss from './Navigation.scss';
import Icon from './icon/Icons';
import logo from '../../../assets/img/logo.png';
import bookmark from '../../../assets/svg/bookmark.svg';
import map from '../../../assets/svg/map.svg';
import lock from '../../../assets/svg/lock.svg';


class Navigation extends React.Component {
  
  state = {
    isTop: true,
  };
  componentDidMount() {
    document.addEventListener('scroll', () => {
      const isTop = window.scrollY < 100;
      if (isTop !== this.state.isTop) {
          this.setState({ isTop })
      }
    });
  }



  render() {

    const alt = 'Plenerowe Kino';
let style = scss.Navigation 

if(!this.state.isTop) {
  style = [scss.Navigation, scss.Scroll].join(' ');
};

    return (
  
  <AUXY>

    <div className={style}>
      <div className={scss.Logo}>
        <Link to="/"><img src={logo} alt="kino plenerowe" /></Link>
      </div>

      <ul>
        <li><NavLink to="/page/filmy" >Filmy</NavLink></li>
        <li><NavLink to="/page/kina" >Kina</NavLink></li>
        <li><NavLink to="/mojalista" >Moja Lista</NavLink></li>


      </ul>

      <ul className={scss.Icons}>
        <Icon myUrl="/page/bookmark" link={bookmark} alt={alt} />
        <Icon myUrl="/page/mapa" link={map} alt={alt} />
        <Icon myUrl="/page/zaloguj" link={lock} alt={alt} />
      </ul>


    </div>
  </AUXY>

)
}};


export default Navigation;
