import React from 'react';
import { NavLink } from 'react-router-dom';
import scss from './Icons.scss';

const Icons = (props) => (

  <NavLink to={props.myUrl} >
    <li className={scss.Icon} >
      <img src={props.link} alt={props.alt} />
    </li>
  </NavLink>

);

export default Icons
