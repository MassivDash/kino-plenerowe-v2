import React from 'react';
import { NavLink } from 'react-router-dom';
import scss from './SideApps.scss';


const sideApps = (props) => (
  <div className={scss.Icons}>
    <NavLink to={props.myUrl}>{props.children}</NavLink>
  </div>

);


export default sideApps;
