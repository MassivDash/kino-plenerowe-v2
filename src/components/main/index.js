import React from 'react';
import { Route, Switch } from 'react-router-dom';

// <Helmet> component for setting the page title/meta tags
import Helmet from 'react-helmet';
import { Redirect } from 'kit/lib/routing';
import { Page, WhenNotFound, Movies } from 'components/routes';

import MainPage from './mainpage/MainPage';
import Mylist from './mylistpage/MyListPage'
import Navigation from './navigation/Navigation';

import Footer from './footer/Footer';
import scss from './main.scss';

export default () => (

  <div className={scss.Container}>
    <Helmet>
      <title>Plenerowe Kino v.2</title>
      <meta name="description" content="Plenerowe Kino App" />
      {/* <base href="http://localhost:8081/" /> */}
    </Helmet>
    <Navigation />
    <Switch>
      <Route exact path="/" component={MainPage} />      
      <Route path="/page/:name" component={Page} />
      <Route path="/mojalista/" component={Mylist} />
      <Route path="/movies/:name" component={Movies} />
      <Redirect from="/old/path" to="/new/path" />
      <Route component={WhenNotFound} />
    </Switch>
    <Footer />
  </div>
);
